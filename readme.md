Custom Arduinos
===============

Mithat Konar

Configuration and bootloader files for some custom Arduinos.

AVR
----
Some conventional AVR-based Arduinos with tweaks to the brown-out detection (BOD). Unless otherwise noted, the bootloaders are from the Arduino 1.8.4 distributables. 

* Pro Mini 3.3V/8MHz, 1.8V BOD, ATmega328P: A 3.3V Pro Mini with a lower 1.8V BOD (ATmega328P only).
* Pro Mini 3.3V/8MHz, no BOD, ATmega328P: A 3.3V Pro Mini with no BOD (ATmega328P only).
* ATmega328P 8MHz int clk, 1.8V BOD: A 3.3V/8MHz Arduino using the ATmega328P's internal oscillator, 1.8V BOD.
* ATmega328P 8MHz int clk, no BOD: A 3.3V/8MHz Arduino using the ATmega328P's internal oscillator, no BOD.
* Pro Mini 5V/16MHz, 4.3V BOD, ATmega328P: A 5V Pro Mini with a higher 4.3V BOD (ATmega328P only).
