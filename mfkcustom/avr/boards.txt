##############################################################
# Arduino Pro Mini 3.3V/8MHz, 1.8V BOD (ATmega328P only)

mfkpro18bod.name=Pro Mini 3.3V/8MHz, 1.8V BOD, ATmega328P

mfkpro18bod.upload.protocol=arduino
mfkpro18bod.upload.maximum_size=30720
mfkpro18bod.upload.maximum_data_size=2048
mfkpro18bod.upload.speed=57600

mfkpro18bod.bootloader.low_fuses=0xFF
mfkpro18bod.bootloader.high_fuses=0xDA
mfkpro18bod.bootloader.extended_fuses=0xFE

mfkpro18bod.bootloader.file=atmega/ATmegaBOOT_168_atmega328_pro_8MHz.hex
mfkpro18bod.bootloader.unlock_bits=0x3F
mfkpro18bod.bootloader.lock_bits=0x0F

mfkpro18bod.build.mcu=atmega328p
mfkpro18bod.build.f_cpu=8000000L
mfkpro18bod.build.core=arduino:arduino
mfkpro18bod.build.variant=arduino:eightanaloginputs
mfkpro18bod.build.board=AVR_PRO

mfkpro18bod.bootloader.tool=arduino:avrdude
mfkpro18bod.upload.tool=arduino:avrdude
##############################################################

##############################################################
# Arduino Pro Mini 3.3V/8MHz, no BOD (ATmega328P only)

mfkpronobod.name=Pro Mini 3.3V/8MHz, no BOD, ATmega328P

mfkpronobod.upload.protocol=arduino
mfkpronobod.upload.maximum_size=30720
mfkpronobod.upload.maximum_data_size=2048
mfkpronobod.upload.speed=57600

mfkpronobod.bootloader.low_fuses=0xFF
mfkpronobod.bootloader.high_fuses=0xDA
mfkpronobod.bootloader.extended_fuses=0xFF

mfkpronobod.bootloader.file=atmega/ATmegaBOOT_168_atmega328_pro_8MHz.hex
mfkpronobod.bootloader.unlock_bits=0x3F
mfkpronobod.bootloader.lock_bits=0x0F

mfkpronobod.build.mcu=atmega328p
mfkpronobod.build.f_cpu=8000000L
mfkpronobod.build.core=arduino:arduino
mfkpronobod.build.variant=arduino:eightanaloginputs
mfkpronobod.build.board=AVR_PRO

mfkpronobod.bootloader.tool=arduino:avrdude
mfkpronobod.upload.tool=arduino:avrdude
##############################################################

##############################################################
# Arduino Pro Mini 3.3V/8MHz, 1.8V BOD, internal clock (ATmega328P only)

mfkprointclk18bod.name=ATmega328P 8MHz int clk, 1.8V BOD

mfkprointclk18bod.upload.protocol=arduino
mfkprointclk18bod.upload.maximum_size=30720
mfkprointclk18bod.upload.maximum_data_size=2048
mfkprointclk18bod.upload.speed=57600

mfkprointclk18bod.bootloader.low_fuses=0xE2
mfkprointclk18bod.bootloader.high_fuses=0xDA
mfkprointclk18bod.bootloader.extended_fuses=0xFE

mfkprointclk18bod.bootloader.file=atmega/ATmegaBOOT_168_atmega328_pro_8MHz.hex
mfkprointclk18bod.bootloader.unlock_bits=0x3F
mfkprointclk18bod.bootloader.lock_bits=0x0F

mfkprointclk18bod.build.mcu=atmega328p
mfkprointclk18bod.build.f_cpu=8000000L
mfkprointclk18bod.build.core=arduino:arduino
mfkprointclk18bod.build.variant=arduino:eightanaloginputs
mfkprointclk18bod.build.board=AVR_PRO

mfkprointclk18bod.bootloader.tool=arduino:avrdude
mfkprointclk18bod.upload.tool=arduino:avrdude
##############################################################

##############################################################
# Arduino Pro Mini 3.3V/8MHz, no BOD, internal clock (ATmega328P only)

mfkprointclknobod.name=ATmega328P 8MHz int clk, no BOD

mfkprointclknobod.upload.protocol=arduino
mfkprointclknobod.upload.maximum_size=30720
mfkprointclknobod.upload.maximum_data_size=2048
mfkprointclknobod.upload.speed=57600

mfkprointclknobod.bootloader.low_fuses=0xE2
mfkprointclknobod.bootloader.high_fuses=0xDA
mfkprointclknobod.bootloader.extended_fuses=0xFF

mfkprointclknobod.bootloader.file=atmega/ATmegaBOOT_168_atmega328_pro_8MHz.hex
mfkprointclknobod.bootloader.unlock_bits=0x3F
mfkprointclknobod.bootloader.lock_bits=0x0F

mfkprointclknobod.build.mcu=atmega328p
mfkprointclknobod.build.f_cpu=8000000L
mfkprointclknobod.build.core=arduino:arduino
mfkprointclknobod.build.variant=arduino:eightanaloginputs
mfkprointclknobod.build.board=AVR_PRO

mfkprointclknobod.bootloader.tool=arduino:avrdude
mfkprointclknobod.upload.tool=arduino:avrdude
##############################################################


##############################################################
# Arduino Pro Mini 5V/16MHz, 4.3V BOD (ATmega328P only)

mfkpro5v43bod.name=Pro Mini 5V/16MHz, 4.3V BOD, ATmega328P

mfkpro5v43bod.upload.protocol=arduino
mfkpro5v43bod.upload.maximum_size=30720
mfkpro5v43bod.upload.maximum_data_size=2048
mfkpro5v43bod.upload.speed=57600

mfkpro5v43bod.bootloader.low_fuses=0xFF
mfkpro5v43bod.bootloader.high_fuses=0xDA
mfkpro5v43bod.bootloader.extended_fuses=0xFC

mfkpro5v43bod.bootloader.file=atmega/ATmegaBOOT_168_atmega328.hex
mfkpro5v43bod.bootloader.unlock_bits=0x3F
mfkpro5v43bod.bootloader.lock_bits=0x0F

mfkpro5v43bod.build.mcu=atmega328p
mfkpro5v43bod.build.f_cpu=16000000L
mfkpro5v43bod.build.core=arduino:arduino
mfkpro5v43bod.build.variant=arduino:eightanaloginputs
mfkpro5v43bod.build.board=AVR_PRO

mfkpro5v43bod.bootloader.tool=arduino:avrdude
mfkpro5v43bod.upload.tool=arduino:avrdude
##############################################################
